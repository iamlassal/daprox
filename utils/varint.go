package utils

import "errors"

func ParseVarInt(b []byte) (uint32, int, error) {
	var result uint32
	for i, byteValue := range b {
		result |= uint32(byteValue&0b01111111) << (i * 7)
		if byteValue&0b10000000 == 0 {
			return result, i + 1, nil
		}
		if i > 4 {
			return 0, 0, errors.New("VarInt too big")
		}
	}
	return 0, 0, errors.New("buffer ended too soon")
}

func hasContinueBit(b byte) bool {
	return b>>7 == 1
}
