package utils

import (
	"bufio"
	"net"
)

type BufConn struct {
	net.Conn
	reader *bufio.Reader
}

func (c BufConn) Peek(n int) ([]byte, error) {
	return c.reader.Peek(n)
}

func (c BufConn) Read(b []byte) (int, error) {
	return c.reader.Read(b)
}

func NewBufConn(c net.Conn) BufConn {
	return BufConn{
		reader: bufio.NewReader(c),
		Conn:   c,
	}
}
