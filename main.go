package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"net.openlasse.daprox/minecraft"
)

func main() {
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGTERM)
	// as of right now the proxy only supports one reroute, due to the TCP listener in the instance
	proxy := minecraft.GetProxy()
	proxy.Start()
	<-stop
	fmt.Println("Stopping...")
	proxy.Stop()
	fmt.Println("daprox stopped")
}
