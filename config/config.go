package config

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"sync"
)

type proxConfig struct {
	Minecraft struct {
		ListenPort string `json:"listenport"`
		Map        []struct {
			Source string `json:"source"`
			Target string `json:"target"`
		} `json:"map"`
	} `json:"minecraft"`
}

const FILENAME = "config.json"

var once sync.Once
var instance *proxConfig

func GetConfig() *proxConfig {
	once.Do(func() {
		instance = &proxConfig{}
		file, err := os.Open(FILENAME)

		if err != nil {
			fmt.Printf("Attempting to generate \"%s\"\n", FILENAME)
			instance.generate()
			stdcfg, err := json.MarshalIndent(instance, "", "\t")
			if err != nil {
				panic(err)
			}
			file, err = os.Create(FILENAME)
			file.WriteString(string(stdcfg))
		}
		b, err := io.ReadAll(file)
		json.Unmarshal(b, instance)
	})
	return instance
}

func (p *proxConfig) generate() {
	p.Minecraft.ListenPort = "25565"
	p.Minecraft.Map = append(p.Minecraft.Map, struct {
		Source string "json:\"source\""
		Target string "json:\"target\""
	}{Source: "",
		Target: ":25566"})
}
