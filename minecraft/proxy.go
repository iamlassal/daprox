package minecraft

import (
	"fmt"
	"io"
	"net"
	"sync"

	"net.openlasse.daprox/config"
	"net.openlasse.daprox/utils"
)

type minecraftReverseProxy struct {
	listener      net.Listener
	listenerMutex sync.Mutex
}

var once sync.Once
var instance *minecraftReverseProxy

func GetProxy() *minecraftReverseProxy {
	once.Do(func() {
		instance = &minecraftReverseProxy{}
	})
	return instance
}

// Target IP is where to send incoming connections
// Source IP is to check what IP the connection came from
func (m *minecraftReverseProxy) Start() {
	go m.start()
}

func (m *minecraftReverseProxy) start() {
	fmt.Println("Minecraft reverse proxy started")
	mcProxyListener, err := net.Listen("tcp", config.GetConfig().Minecraft.ListenPort)
	if err != nil {
		panic(err)
	}
	defer mcProxyListener.Close()

	for {
		clientConn, err := mcProxyListener.Accept()
		if err != nil {
			fmt.Println("Error: ", err)
			continue
		}

		bufferedConn := utils.NewBufConn(clientConn)

		handshake, err := parseHandshake(&bufferedConn)
		if err != nil {
			fmt.Println("could not parse handshake")
			continue
		}

		isValid := false
		var target string
		for _, ipMap := range config.GetConfig().Minecraft.Map {
			if ipMap.Source == handshake.hostname {
				target = ipMap.Target
				isValid = true
			}
		}

		if !isValid {
			fmt.Printf("client %s connected from invalid source: %s\n", clientConn.RemoteAddr().String(), handshake.hostname)
			continue
		}

		minecraftConn, err := net.Dial("tcp", target)
		if err != nil {
			fmt.Println("Error connecting to Minecraft server:", err)
			continue
		}
		go m.transferData(bufferedConn, minecraftConn)
		go m.transferData(minecraftConn, bufferedConn)
	}
}

func (m *minecraftReverseProxy) Stop() {
	m.listenerMutex.Lock()
	defer m.listenerMutex.Unlock()
	if m.listener != nil {
		m.listener.Close()
	}
}

func (m *minecraftReverseProxy) transferData(src, dst net.Conn) {
	defer src.Close()
	defer dst.Close()

	buffer := make([]byte, 1024)
	for {
		n, err := src.Read(buffer)
		if err != nil {
			if err != io.EOF {
				return
			}
			return
		}

		_, err = dst.Write(buffer[:n])
		if err != nil {
			fmt.Println("Error writing to destination:", err)
			return
		}
	}
}
