package minecraft

import (
	"net.openlasse.daprox/utils"
)

type handshake struct {
	identifier int
	version    int
	hostname   string
}

func parseHandshake(c *utils.BufConn) (*handshake, error) {
	id, ver, host, err := parsePacket(c)
	if err != nil {
		return nil, err
	}
	return &handshake{
		identifier: id,
		version:    ver,
		hostname:   host,
	}, nil
}

func parsePacket(c *utils.BufConn) (int, int, string, error) {
	initial, err := c.Peek(5)
	if err != nil {
		return 0, 0, "", err
	}

	var offset int
	var lastCount int

	pSize, offset, err := utils.ParseVarInt(initial)
	packet, err := c.Peek(int(pSize))
	if err != nil {
		return 0, 0, "", err
	}

	identifier, lastCount, err := utils.ParseVarInt(packet[offset:])
	if err != nil {
		return 0, 0, "", err
	}
	offset += lastCount

	version, lastCount, err := utils.ParseVarInt(packet[offset:])
	if err != nil {
		return 0, 0, "", err
	}
	offset += lastCount

	strLen, lastCount, err := utils.ParseVarInt(packet[offset:])
	if err != nil {
		return 0, 0, "", err
	}
	offset += lastCount

	hostname := packet[offset : offset+int(strLen)]

	return int(identifier), int(version), string(hostname), nil
}
